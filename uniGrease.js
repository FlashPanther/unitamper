// ==UserScript==
// @name         UniTamper
// @namespace    http://www.tdevos.be/
// @version      2.1
// @description  Super description unusefull
// @author       Thomas Devos
// @match        http://hhrmatriuap005v/uniweb/code/prest/prest_list_hist.asp
// @require      http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js
// @require      https://raw.githubusercontent.com/nnnick/Chart.js/v0.2.0/Chart.js
// @grant        none
// ==/UserScript==

$(function(){
    
    var toSeconds = function(time) {
    	var parts = time.split(':');
    	var seconds = ( + parts[0]) * 60 * 60 + ( + parts[1]) * 60;
    	if (time.indexOf('-') > - 1)
    		seconds = -seconds;
    	return seconds;
  	}
    
    var toDecimal = function(time){
        var seconds = toSeconds(time);
        var tempTime = Math.round(seconds / 36) / 100;
        
        var integer = Math.floor(tempTime);
        var tempFloat = tempTime - integer;
        
        var float = Math.round(tempFloat * 100);
        if(float < 10)
            var float = "0" + float;
        if(float < 1)
            var float = "00";
        
        return integer + "," + float;
    }
    
    var toHHMM = function(sec, withSeconds){
        var negative = false;
	    if (sec < 0) {
      		negative = true;
      		sec = sec * - 1;
    	}
    	var sec_num = parseInt(sec, 10);
    	var hours = Math.floor(sec_num / 3600);
    	var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    	var seconds = Math.floor(sec_num - (hours * 3600) - (minutes * 60));
    	if (hours < 10) {
      		hours = '0' + hours;
    	}
    	if (minutes < 10) {
      		minutes = '0' + minutes;
    	}
    	if (seconds < 10) {
      		seconds = '0' + seconds;
    	}
    	if(withSeconds){
      		var time = hours + ':' + minutes + ':' + seconds;
    	}else{
      		var time = hours + ':' + minutes;
    	}
    
    	if (negative)
    		time = '-' + time;
    	return time;
    }
    
    var parseHours = function(){
        var hours = [];
        $("table tr td:nth-child(15)").each(function(){
            var matchedHour = $(this).html().match(/[0-9]:[0-9]+/g);
            if(matchedHour != null){
				hours.push(toSeconds(matchedHour[0]));
                $(this).append("(" + toDecimal(matchedHour[0]) + ")");
            }
        });
        return hours;
    }
    
    var calculateSolde = function(hours){
        var total = 0;
        $.each(hours, function(id, value){
            total = total + value;
        });
        return total - (hours.length * 28800);
    }
    
    var hours = parseHours();
    var solde = calculateSolde(hours);
    
    $("#table > tbody > tr:nth-last-child(8) td:nth-child(5)").attr({"colspan": "1"}).after($("<td>").attr({"bgcolor" : "#166A9C", "align" : "right"}).addClass("header").html($("<b>").html("Solde")));
    
    $("#table > tbody > tr:nth-last-child(6) td:nth-child(5)").attr({"colspan": "1"}).after($("<td>").attr({"bgcolor" : "#DDDEAB", "align" : "right"}).html($("<b>").html(toHHMM(solde, false))));
});
